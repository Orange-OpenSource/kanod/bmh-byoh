# bmh-byoh
A controller to set annotation on bareMetalHost indicating if the corresponding byohost
is used by a byoMachine.

## Description
When `Bring Your Own Host` Clusterapi provider is  used with the broker, ByoHost are created by preprovisionning bareMetalHost with custom userData.

When a ByoHost is referenced by a ByoMachine, a label `byoh.infrastructure.cluster.x-k8s.io/byomachine-name` is put on this ByoHost. The bmh-byoh controller then adds the `kanod.io/bmh-used-by-byohmachine` annotation with value as `true` on the corresponding BareMetalHost.

If a ByoHost is not referenced by a ByoMachine, the bmh-byoh controller add the `kanod.io/bmh-used-by-byohmachine` annotation with value as `false` on the corresponding BareMetalHost

## License

Copyright 2023 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
