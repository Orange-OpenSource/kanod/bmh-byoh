/*
Copyright 2023 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"

	"github.com/go-logr/logr"
	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	apiextensions "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/client-go/util/retry"

	byohv1beta1 "gitlab.com/Orange-OpenSource/kanod/bmh-byoh/internal/byoh_api/v1beta1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	BYOH_MACHINE_LABEL  = "byoh.infrastructure.cluster.x-k8s.io/byomachine-name"
	BMH_BYOH_ANNOTATION = "kanod.io/bmh-used-by-byohmachine"
	BYOH_POOL_LABEL     = "kanod.io/pool"
)

// ByoHostReconciler reconciles a ByoHost object
type ByoHostReconciler struct {
	client.Client
	Scheme    *runtime.Scheme
	byohWatch bool
}

//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byohosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byohosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=apiextensions.k8s.io,resources=customresourcedefinitions,verbs=get;list;watch;create;update;patch

func (r *ByoHostReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.Log.WithValues("bmh-byoh", req.NamespacedName)
	isUsedByByohMachine := false
	byoHost := &byohv1beta1.ByoHost{}
	err := r.Get(ctx, req.NamespacedName, byoHost)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("byoHost resource not found. Ignoring")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get ByoHost")
		return ctrl.Result{}, err
	}

	if _, ok := byoHost.Labels[BYOH_MACHINE_LABEL]; ok {
		isUsedByByohMachine = true
	}

	bareMetalHost := &bmhv1alpha1.BareMetalHost{}
	err = r.Get(ctx, req.NamespacedName, bareMetalHost)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			// if this byoHost is associated with a bareMetalHost created by bareMetalPool
			// we delete it as the bareMetalHost do not exists anymore
			if _, ok := byoHost.Labels[BYOH_POOL_LABEL]; ok {
				err = r.Delete(ctx, byoHost)
				if err != nil {
					log.Error(err, "Failed to delete ByoHost", "byoHost", byoHost.Name)
					return ctrl.Result{}, err
				} else {
					log.Info("Delete ByoHost", "byoHost", byoHost.Name)
					return ctrl.Result{}, nil
				}
			}

			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get BareMetalHost")
		return ctrl.Result{}, err
	}

	err = r.UpdateBmhAnnotations(&ctx, byoHost, bareMetalHost, isUsedByByohMachine, log)
	if err != nil {
		log.Error(err, "Cannot update bareMetalHost annotation", "bareMetalHost", bareMetalHost.Name)
	}

	return ctrl.Result{}, nil
}

func hasCrd(mgr ctrl.Manager, name string) bool {
	crd := &apiextensions.CustomResourceDefinition{}
	key := types.NamespacedName{Name: name}
	ctx := context.Background()
	err := mgr.GetAPIReader().Get(ctx, key, crd)
	if err != nil {
		logger.Log.Error(err, "Cannot find CRD", "crd", name)
	}
	return err == nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ByoHostReconciler) SetupWithManager(mgr ctrl.Manager) error {
	controller := ctrl.NewControllerManagedBy(mgr)

	if hasCrd(mgr, "byohosts.infrastructure.cluster.x-k8s.io") {
		r.byohWatch = true
		logger.Log.Info("Support for ByoHost: ok")
		controller = controller.For(&byohv1beta1.ByoHost{})
	} else {
		logger.Log.Info("Support for ByoHost: fail - Not watching ByoHosts")
	}

	controller = controller.Watches(
		&bmhv1alpha1.BareMetalHost{},
		handler.EnqueueRequestsFromMapFunc(r.BareMetalHostToByoHost))

	return controller.Complete(r)
}

// BareMetalHostToByoHost will return a reconcile request for a ByoHost if the event is for a
// BareMetalHost. BareMetalHost and ByoHost have the same name.
func (r *ByoHostReconciler) BareMetalHostToByoHost(ctx context.Context, obj client.Object) []ctrl.Request {
	requests := []ctrl.Request{}

	if r.byohWatch {
		if bmh, ok := obj.(*bmhv1alpha1.BareMetalHost); ok {
			requests = append(requests, ctrl.Request{
				NamespacedName: types.NamespacedName{
					Name:      bmh.Name,
					Namespace: bmh.Namespace,
				},
			})
		}
	}
	return requests
}

func (r *ByoHostReconciler) UpdateBmhAnnotations(
	ctx *context.Context,
	byoHost *byohv1beta1.ByoHost,
	bareMetalHost *bmhv1alpha1.BareMetalHost,
	isUsedByByohMachine bool,
	log logr.Logger,
) error {
	key := client.ObjectKey{
		Name:      bareMetalHost.Name,
		Namespace: bareMetalHost.Namespace,
	}

	bmh := &bmhv1alpha1.BareMetalHost{}

	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := r.Get(*ctx, key, bmh); err != nil {
				return err
			}

			initialBmhAnnotations := bmh.GetAnnotations()
			updatedBmhAnnotations := make(map[string]string)

			for key, value := range initialBmhAnnotations {
				updatedBmhAnnotations[key] = value
			}

			if isUsedByByohMachine {
				updatedBmhAnnotations[BMH_BYOH_ANNOTATION] = "true"
			} else {
				updatedBmhAnnotations[BMH_BYOH_ANNOTATION] = "false"
			}

			if !mapEqual(updatedBmhAnnotations, initialBmhAnnotations) {
				bmh.SetAnnotations(updatedBmhAnnotations)
				log.Info("Updating baremetalhost annotations", "name", bmh.Name, "namespace", bmh.Namespace, "annotation", updatedBmhAnnotations)
				return r.Update(*ctx, bmh)
			}
			return nil
		})
	if err != nil {
		log.Error(err, "Cannot update baremetalhost.")
	}
	return err
}

// mapEqual compares two maps.
func mapEqual(m1 map[string]string, m2 map[string]string) bool {
	if len(m1) != len(m2) {
		return false
	}
	for k1, e1 := range m1 {
		e2, ok := m2[k1]
		if !ok || e2 != e1 {
			return false
		}
	}
	return true
}
