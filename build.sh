#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

GOLANG_VERSION=1.20

for var in NEXUS_REGISTRY REPO_URL IMAGE_SYNC_DIR VERSION
do
    if ! [[ -v "${var}" ]]; then
        echo "${var} must be defined"
        exit 1
    fi
done

declare -a proxy_args
declare -a proxy_run
if [ -n "${http_proxy:-}" ]; then
    proxy_args+=(--build-arg "http_proxy=${http_proxy}")
    proxy_run+=(--env "HTTP_PROXY=${http_proxy}")
fi
if [ -n "${https_proxy:-}" ]; then
    proxy_args+=(--build-arg "https_proxy=${https_proxy}")
    proxy_run+=(--env "HTTPS_PROXY=${https_proxy}")
fi
if [ -n "${no_proxy:-}" ]; then
    proxy_args+=(--build-arg "no_proxy=${no_proxy}")
    proxy_run+=(--env "NO_PROXY=${no_proxy}")
fi
if [ -n "${GOPROXY:-}" ]; then
    proxy_args+=(--build-arg GOPROXY)
    proxy_run+=(--env GOPROXY)
fi

export kustomizeVersion="3.8.5"
export CONTAINER=bmh-byoh

docker run --rm "${proxy_run[@]}" -v "$PWD":/usr/src/myapp -w /usr/src/myapp "golang:${GOLANG_VERSION}" make manifests

if [ ! -f kustomize ]; then
    curl -L -o kustomize.tgz https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${kustomizeVersion}/kustomize_v${kustomizeVersion}_linux_amd64.tar.gz
    tar -zxvf kustomize.tgz kustomize
    rm kustomize.tgz
fi

pushd config/manager && ../../kustomize edit set image "controller=${NEXUS_REGISTRY_INTERNAL}/${CONTAINER}:${VERSION}" && popd
./kustomize build config/default -o bmh-byoh.yml

if [ -n "${MANIFEST_ONLY:-}" ]; then
    exit 0
fi

# bash ./ci-upload.sh

docker run --rm "${proxy_run[@]}" -v "$PWD":/usr/src/myapp -w /usr/src/myapp "golang:${GOLANG_VERSION}" make generate

docker build "${proxy_args[@]}" . -t "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"

echo "${NEXUS_KANOD_PASSWORD}" | docker login -u "${NEXUS_KANOD_USER}" --password-stdin "${NEXUS_REGISTRY}"

docker push "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"

if [ "${KANOD_PRUNE_IMAGES:-0}" == "1" ]; then
    docker image prune -a --force --filter 'label=project=bmh-byoh'
fi

