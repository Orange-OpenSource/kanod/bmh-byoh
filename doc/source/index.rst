========
Bmh-Byoh
========

.. toctree::
    :maxdepth: 2

Overview
========

When `Bring Your Own Host` Clusterapi provider is  used with the broker, ByoHost are created by preprovisionning bareMetalHost with custom userData.

When a ByoHost is referenced by a ByoMachine, a label `byoh.infrastructure.cluster.x-k8s.io/byomachine-name` is put on this ByoHost.
The bmh-byoh controller then adds the `kanod.io/bmh-used-by-byohmachine` annotation with value as `true` on the corresponding BareMetalHost.

If a ByoHost is not referenced by a ByoMachine, the bmh-byoh controller add the `kanod.io/bmh-used-by-byohmachine` annotation with value as `false` on the corresponding BareMetalHost
